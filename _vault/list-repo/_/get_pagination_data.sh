#!/bin/bash
_SH=$(cd `dirname $BASH_SOURCE` && pwd)
source "$_SH/common.sh"

    r=$PAGINATION_DATA  # r aka result
    t=`mktemp`
        curl -s --head  --header "PRIVATE-TOKEN:$GITLAB_PERSONAL_ACCESS_TOKEN" 'https://gitlab.com/api/v4/projects?owned=true&per_page=100' | tee $t
        #       !       .                                                      .                                   .          !
            x_total_page=$(cat $t | grep 'x-total-pages')  # get total page of gitlab project ref. https://stackoverflow.com/a/49127473/248616
              x_per_page=$(cat $t | grep 'x-per-page')

            sed 's/^[[:space:]]*//' <<< "
                $x_total_page
                $x_per_page
            " > $r  # dedent in bash ref. https://stackoverflow.com/a/37888919/248616

                sed -i '/^$/d' $r  # remove blank line ref https://serverfault.com/a/252925/41015
