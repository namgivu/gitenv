#!/bin/bash
_SH=$(cd `dirname $BASH_SOURCE` && pwd)
GITENV_HOME=$(cd "$_SH/../../.." && pwd)

    PAGINATION_DATA="$GITENV_HOME/_vault/list-repo/.data/pagination"
          REPO_DATA="$GITENV_HOME/_vault/list-repo/.data/list-repo.json"

    [ -f "$GITENV_HOME/.env" ] && source "$GITENV_HOME/.env" || (echo 'File .env not found; please clone one from .template.env'; kill $$; exit 1)
