#!/bin/bash
_SH=$(cd `dirname $BASH_SOURCE` && pwd)
source "$_SH/common.sh"

[ ! -f $PAGINATION_DATA ] && (echo "File not found $PAGINATION_DATA"; kill $$; exit 1)

    #                                                                           ! trim spaces ref. https://unix.stackexchange.com/a/32587/17671
    x_total_page=$(cat $PAGINATION_DATA | grep 'x-total-page' | cut -d':' -f2 | tr -d [:space:])
      x_per_page=$(cat $PAGINATION_DATA | grep 'x-per-page'   | cut -d':' -f2 | tr -d [:space:])

    r=$REPO_DATA
    rm -rf $r  # clear data file
        for ((p=1; p<=$x_total_page; p++)); do  # loop n times in bash ref. https://stackoverflow.com/a/3737771/248616
            echo "Getting repo at page=$p/$x_total_page ..."
                # api endpoint to list gitlab project ref. https://stackoverflow.com/a/45087988/248616
                curl -s --header "PRIVATE-TOKEN:$GITLAB_PERSONAL_ACCESS_TOKEN" "https://gitlab.com/api/v4/projects?owned=true&per_page=$x_per_page&page=$p" | python -m json.tool > $r
                #    .  !                                                       .                                  !          !                    !
                #                                                                                                  =only show my project ref. https://stackoverflow.com/a/52999232/248616
        done
