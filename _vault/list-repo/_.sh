#!/bin/bash
docstring='
NO_CACHE=1 ./_.sh
NO_CACHE=0 ./_.sh
           ./_.sh
'

SH=$(cd `dirname $BASH_SOURCE` && pwd)
source "$SH/_/common.sh"

    if [ "$NO_CACHE" == "1" ]; then  # make call api to get data
        source "$SH/_/get_pagination_data.sh"
        source "$SH/_/get_repo_data.sh"
    fi

        sed 's/^[[:space:]]*//' <<<  "
            --- data retrieved, start query from here
            x_total_page=$(cat $PAGINATION_DATA | grep 'x-total-page' | cut -d':' -f2 | xargs)
              x_per_page=$(cat $PAGINATION_DATA | grep 'x-per-page'   | cut -d':' -f2 | xargs)
        "  # dedent in bash ref. https://stackoverflow.com/a/37888919/248616

        JSON_DATA_FILE=$REPO_DATA python "$SH/_.py"
