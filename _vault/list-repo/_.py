import json
import os
from pprint import pprint

"""
sample usage
JSON_DATA_FILE=./log.json python ./list-repo.py 
"""

JSON_DATA_FILE = os.environ.get('JSON_DATA_FILE')
if not JSON_DATA_FILE:
    print('Envvar JSON_DATA_FILE not found')

with open(JSON_DATA_FILE) as f:
    j = json.load(f)

gitlab_repo_items = j
repo_list = []
for i in gitlab_repo_items:
    repo_list.append({'name': i.get('name')})

pprint(repo_list)
print('\n'.join([r['name'] for r in repo_list]))
